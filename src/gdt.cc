namespace fast_os
{

namespace
{

inline void gdt_reload() noexcept
{
	__asm__ __volatile__
	(
        "lgdt %0\n\t"
        "push 0x08\n\t"
        "lea rax, [rip + OFFSET 1f]\n\t"
        "push rax\n\t"
        "lretq\n\t"
        "1:\n\t"
        "mov eax, 0x10\n\t"
        "mov ds, ax\n\t"
        "mov es, ax\n\t"
        "mov fs, ax\n\t"
        "mov gs, ax\n\t"
        "mov ss, ax\n\t"
        :
        : "m"(gdt_pointer)
        : "rax", "memory"
    	);
}

}
}

namespace fast_os
{

extern void gdt_init() noexcept
{
	auto& entries{gdt.entries};
	*entries={};
	entries[1]={.access=0b10011010,.granularity=0b00100000};
	entries[2]={.access=0b10010010};
	entries[3]={.access=0b11110010};
	entries[4]={.access=0b00100000};
	auto& tss{gdt.tss};
	tss={.length=104,.flags1=0b10001001};
	gdt_pointer.size = sizeof(gdt)-1;
	gdt_pointer.address = static_cast<std::uint64_t>(__builtin_bit_cast(std::uintptr_t,__builtin_addressof(gdt)));
	gdt_reload();
}

}
