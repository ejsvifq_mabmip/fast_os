#pragma once

namespace fast_os
{

struct
[[gnu::packed]]
gdt_entry
{
	std::uint16_t limit{};
	std::uint16_t base_low16{};
	std::uint8_t  base_mid8{};
	std::uint8_t  access{};
	std::uint8_t  granularity{};
	std::uint8_t  base_high8{};
};

struct
[[gnu::packed]]
tss_entry
{
	std::uint16_t length{};
	std::uint16_t base_low16{};
	std::uint8_t base_mid8{};
	std::uint8_t flags1{};
	std::uint8_t flags2{};
	std::uint8_t base_high8{};
	std::uint32_t base_upper32{};
	std::uint32_t reserved{};
};

struct
[[gnu::packed]]
gdt_t
{
	gdt_entry entries[5]{};
	tss_entry tss{};
};

struct
[[gnu::packed]]
gdt_pointer_t
{
	std::uint16_t size{};
	std::uint64_t address{};
};

extern gdt_t gdt;
extern gdt_pointer_t gdt_pointer;

extern void gdt_init() noexcept;
extern void gdt_load_tss(std::uintptr_t addr) noexcept;

}
