#pragma once

#include<atomic>

namespace fast_os
{

inline void spin_lock_lock(std::atomic_flag& lock) noexcept
{
	while(lock.test_and_set(std::memory_order_acquire))
		while(lock.test(std::memory_order_acquire));
}

inline void spin_lock_unlock(std::atomic_flag& lock) noexcept
{
	lock.clear(std::memory_order_release);
}

struct spin_lock
{
	std::atomic_flag lk = ATOMIC_FLAG_INIT;
	void lock() noexcept
	{
		spin_lock_lock(lk);
	}
	void unlock() noexcept
	{
		spin_lock_unlock(lk);
	}
};

template<typename mutex_type>
struct lock_guard
{
mutex_type& mutex;
explicit constexpr lock_guard(mutex_type& m) noexcept : mutex{m}
{
	mutex.lock();
}

#if __cpp_constexpr >= 201907L
constexpr
#endif
~lock_guard() noexcept
{ mutex.unlock(); }
lock_guard(lock_guard const&) = delete;
lock_guard& operator=(lock_guard const&) = delete;
};
}
