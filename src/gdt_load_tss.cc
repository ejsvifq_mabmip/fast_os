namespace fast_os
{

namespace
{
spin_lock lock;
}

extern void gdt_load_tss(std::uintptr_t addr) noexcept
{
	lock_guard guard{lock};
	gdt.tss={.base_low16=static_cast<std::uint16_t>(addr),
		.base_mid8=static_cast<std::uint8_t>(addr >> 16),
		.flags1=0b10001001,
		.base_high8=static_cast<std::uint8_t>(addr>>24),
		.base_upper32=static_cast<std::uint32_t>(addr>>32)};
	__asm__ __volatile__ ("ltr %0" : : "rm" ((std::uint16_t)0x28) : "memory");
}

}
