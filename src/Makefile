KERNEL := kernel.elf

CC = x86_64-elf-gcc
CXX = x86_64-elf-g++
LD = x86_64-elf-ld

CXXFLAGS = -Wall -Wextra -pipe -fno-exceptions -fno-rtti -fno-unwind-tables -fno-asynchronous-unwind-tables -std=c++20 -O2 -ffreestanding
LDFLAGS =

INTERNALLDFLAGS :=         \
	-Tlinker.ld            \
	-zmax-page-size=0x1000 \
	-Wl,-static            \
	-Wl,-pie               \
	-Wl,--no-dynamic-linker    \
	-ztext \
	-nostartfiles

INTERNALCFLAGS  :=       \
	-I.                  \
	-fno-stack-protector \
	-fno-pic -fpie       \
	-mno-80387           \
	-mno-mmx             \
	-mno-3dnow           \
	-mno-sse             \
	-mno-sse2            \
	-mno-red-zone

CXXFILES := $(shell find ./ -type f -name '*.cc')
OBJ    := $(CXXFILES:.cc=.o)

.PHONY: all clean

all: $(KERNEL)

stivale2.h:
	wget https://github.com/stivale/stivale/raw/master/stivale2.h

$(KERNEL): $(OBJ)
	$(CXX) $(OBJ) $(LDFLAGS) $(INTERNALLDFLAGS) -o $@ -s -flto

pch.hpp.gch: stivale2.h pch.hpp
	$(CXX) $(CXXFLAGS) $(INTERNALCFLAGS) -c pch.hpp -flto
%.o: %.cc pch.hpp.gch
	$(CXX) $(CXXFLAGS) $(INTERNALCFLAGS) -c $< -o $@ -include pch.hpp -flto -s

clean:
	rm -rf $(KERNEL) $(OBJ)

distclean:
	rm -rf $(KERNEL) $(OBJ) pch.hpp.gch
